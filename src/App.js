import React, { useState } from "react";
import { todos as todosList } from "./todos";
import { v4 as uuid} from "uuid"

function App (){
 
  const [todos, setTodos] = useState(todosList)
  const [inputText,setInputText] = useState("")

  const handleAddToDo = (event) => {
    if(event.which === 13){
      const newId = uuid()
      const newToDo = {
        "userId": 1,
        "id": newId,
        "title": inputText,
        "completed": false
      }
      const newToDos = {
        ...todos,
      }
      newToDos[newId] = newToDo
      setTodos(newToDos)
      setInputText("")
    }
  }

  const handleToggle = (id) => {
    const newToDos = {...todos}
    newToDos[id].completed = !newToDos[id].completed
    setTodos(newToDos)
  }

  const handleDeleteTodo = (id) => {
    const newToDos = {...todos}
    delete newToDos[id]
    setTodos(newToDos)

  }
  
  const handleClearCompletedToDos = ()=> {
    const newToDos = {...todos}
    for(const todo in newToDos){
      if(newToDos[todo].completed){
        delete newToDos[todo]
      }
    }
    setTodos(newToDos)
  }

    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          onChange={(event)=>setInputText(event.target.value)}
          onKeyDown={(event)=>handleAddToDo(event)}
          className="new-todo" 
          value={inputText}
          placeholder="What needs to be done?" 
          autofocus />
        </header>
        <TodoList 
        todos={Object.values(todos)} 
        handleToggle = {handleToggle}
        handleDeleteTodo = {handleDeleteTodo}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
          className="clear-completed"
          onClick={()=> handleClearCompletedToDos()}
          >Clear completed</button>
        </footer>
      </section>
    );
  }


function TodoItem(props) {
    return (
      <li className={props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          checked={props.completed} 
          onChange={()=> props.handleToggle(props.id)}
          />
          <label>{props.title}</label>
          <button 
          className="destroy" 
          onClick={()=> props.handleDeleteTodo(props.id)}
          />
        </div>
      </li>
    );
}

function TodoList (props) {
  
    return (
      <section className="main">
        <ul className="todo-list">
          {props.todos.map((todo) => (
            <TodoItem 
            title={todo.title} 
            completed={todo.completed} 
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDeleteTodo={props.handleDeleteTodo}
            key={todo.id}
            />
          ))}
        </ul>
      </section>
    );  
}

export default App;
